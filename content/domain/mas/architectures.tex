\subsubsection{Подходы к построению мультиагентных систем}
\label{sec:mas-arch}


В настоящее время принято выделять три подхода к решению сложных задач и,
соответственно, три типа решающих систем \cite{Organizational}:


\begin{enumerate}
    \item
        централизованный;
    \item
        распределённый;
    \item
        смешанный.
\end{enumerate}


Все эти подходы используются при построении мультиагентных систем
искусственного интеллекта. Каждый из указанных подходов имеет свою область
использования. Они не являются конкурирующими подходами к решению сложных
задач. Предметом исследования мультиагентных систем независимо от используемого
подхода к решению задач является организационная структура, образуемая в
результате организационно-управленченского взаимодействия членов некоторого
сообщества при решении исходной задачи.

Особенностью применения централизованного подхода является то, что дерево целей
для задачи, решаемой агентным коллективом, должно быть задано априори.
Централизованный подход оказывается пригодным для решения таких задач, для в
формулировке которых нет неопределенности по статической структуре исследуемого
оригинала и соответственно всех более «старших» видов неопределенностей. Можно
сказать, что системы с централизованной стратегией управления по функциональным
возможностям и области решаемых задач близки к классическим экспертным
системам.

Возможность априорного задания целей обуславливает важную особенность таких
систем. Системы, построенные по принципу централизованного подхода, прежде
всего, характеризуются наличием в составе системы модуля, исполняющего роль
центрального координатора действий этой системы. Центральный координатор
производит редукцию исходной задачи на более простые подзадачи, т.е. определяет
путь решения исходной задачи. Планы индивидуальных агентов создаются раздельно,
а затем отправляются в «центральный координатор», анализируются им,
идентифицируются потенциальные взаимодействия, которые могут привести к
конфликтам, и группируются в последовательность опасных ситуаций. Затем,
координатор вставляет коммуникационные команды в индивидуальные планы, что
позволяет агентам синхронизировать свою работу. То есть, каждый агент рассылает
координатору сообщения о планируемых действиях. Координатор строит план,
который определяет для каждого агента его действия, включая действия ухода от
коллизий (рис. \ref{fig:mas-arch-centr}). Значит, для централизованной схемы
управления коллективом агентов априори известны (могут быть определены) как
коллектив агентов, так и их связи. В процессе решения конкретной исходной
задачи конкретизируются лишь частные цели агентов


\begin{figure}[h!]
    \centering \includegraphics[scale=0.5]{graphics/mas-arch-centr.png}
    \caption{Централизованная мультиагентная система}
    \label{fig:mas-arch-centr}
\end{figure}


Как видно при централизованном подходе основная нагрузка по построению плана
решения задачи, стоящей перед системой, возлагается на центральный координатор,
а отдельные агенты лишь реализуют достижение необходимых локальных целей.
Централизованный подход к решению задач оказывается вполне пригодным и
достаточным для решения задач умеренной сложности в условиях неизменности
внешней среды. Однако необходимость решения более сложных задач, в формулировке
которых присутствуют неопределенности свыше параметрической и
функционально-структурной приводит к необходимости децентрализации процесса
управления системой.


Концепция распределённых систем (децентрализованного управления) в общих чертах
представляет собой следующее. В некоторой среде функционирует некоторая
достаточная с точки зрения решаемых задач совокупность независимых решающих
систем (агентов). В этой же среде заданы некоторые правила (ограничения)
поведения агентов и задана некоторая глобальная цель. Каждый из агентов,
удовлетворяя ограничениям среды, преследуя свою локальную цель и согласуя свои
действия с другими агентами коллектива -- участвует в процессе достижении
коллективом некоторой глобальной цели. Если глобальную цель преследует
необходимый и достаточный коллектив агентов, то она, безусловно, будет
достигнута (она может быть представлена как состоящая из частных целей агентов
коллектива). Глобальная цель состоит из частных целей в том смысле, что
достижение каждым из агентов своей частной цели приводит к достижению
глобальной цели коллективом агентов.

Первоначально, в ответ на формулировку конкретной глобальной цели откликаются
(готовы с ней работать) несколько агентов, но с учётом действующих в среде
ограничений остаётся один агент. Роль оставшегося агента -- упростить исходную
цель: представить её совокупностью подцелей. Формулировка подцелей приводит к
новому конкурсу среди агентов. Этот процесс продолжается до тех пор, пока не
будут сформулированы подцели (подзадачи), решение которых становится доступным
для соответствующих агентов коллектива.

Таким образом, основным признаком задач, на решение которых ориентированы
системы с децентрализованной координацией агентов, является невозможность
априори редуцировать решаемые системой задачи. Результатом достижения
коллективом агентов конкретной глобальной цели являются её редукция на подцели,
определение конкретного коллектива агентов, обеспечивающего достижение
конкретной глобальной цели, структура связей между агентами, формируемая на
период решения конкретной исходной задачи, и собственно решение исходной задачи
(рис. \ref{fig:mas-arch-distr}).


\begin{figure}[h!]
    \centering \includegraphics[scale=0.5]{graphics/mas-arch-distr.png}
    \caption{Распределённая мультиагентная система}
    \label{fig:mas-arch-distr}
\end{figure}


С точки зрения проблемы планирования при децентрализованной стратегии
управления план создается несколькими агентами. Т.е. отсутствует единый,
центральный координатор могущий оценить действия системы в целом. Тогда
неизбежным следствием децентрализованного планирования является возможность
возникновения конфликтов в многоагентной среде. Например, некоторый агент может
сформулировать цель, которая будет конфликтовать с целями других агентов.
Поэтому обнаружение и разрешение нежелательных взаимодействий при
децентрализованной стратегии управления коллективом агентов становится
значительно более трудным по сравнению с централизованным подходом.


Что касается смешанного подхода к стратегии управления коллективом, то его
сущность, как следует из названия, заключается в некоторой комбинации двух выше
рассмотренных подходов (централизованного и распределенного). Превалирование в
решении задачи характерных особенностей каждого из названных подходов
определяется объемом и полнотой имеющегося в системе знания о структуре дерева
целей. Т.е. чем большим знанием о последовательности и взаимозависимости
подцелей мы располагаем, тем больше в системе присутствуют элементы
центрального планирования с присущими им достоинствами. И обратно: уменьшение
знания ведет к большей децентрализации.

Так, к примеру, если проблемная область искусственной интеллектуальной системы
может быть структурирована априори, то состав коллектива агентов, частные цели
для каждого из агентов и их связи могут быть определены априори. При этом
управление агентами носит иерархический характер. Причём в процессе достижения
конкретной глобальной цели обобщённая иерархия агентов конкретизируется до
конкретного дерева, то есть апостериори определяется только коллектив агентов,
реализующий решение конкретной задачи (рис. \ref{fig:mas-arch-hybrid}).

В другом неопределенность случае если в формулировке задачи присутствует по
статической структуре оригинала, однако, мы располагаем знанием об уровнях
абстракции оригинала, т.е. нам известна глубина дерева. Невозможно априори
определить коллектив агентов, необходимый для решения задачи, и частные цели
для каждого агента.

Однако, возможно упорядочить всех имеющихся в системе агентов по уровням
абстракции. Это означает, что горизонтальные связи могут быть установлены
только между агентами одинакового уровня абстракции, а планы нижестоящего
агента должны быть согласованны с вышестоящим агентом.


\begin{figure}[h!]
    \centering \includegraphics[scale=0.5]{graphics/mas-arch-hybrid.png}
    \caption{Распределённая мультиагентная система с иерархической структурой}
    \label{fig:mas-arch-hybrid}
\end{figure}


Подведем некоторые итоги рассуждений относительно стратегий управления
мультиагентными коллективами.

Для централизованной схемы управления коллективом агентов априори известны
(могут быть определены) как коллектив агентов, так и их связи. В процессе
решения конкретной исходной задачи конкретизируются лишь частные цели агентов.
Однако необходимость решения задач при наличии в их формулировке
неопределенности свыше параметрической и функционально-структурной (т.е.
неопределенности хотя бы по статической структуре оригинала) приводит к
необходимости децентрализации процесса управления системой.

Основным признаком задач, на решение которых ориентированы системы с
невозможность децентрализованной априори координацией редуцировать решаемые
агентов, системой является задачи. Результатом достижения коллективом агентов
конкретной глобальной цели являются


\begin{enumerate}
    \item
        её редукция на подцели;
    \item
        определение конкретного коллектива агентов, обеспечивающего достижение
        конкретной глобальной цели;
    \item
        структура связей между агентами, формируемая на период решения
        конкретной исходной задачи;
    \item
        собственно решение исходной задачи.
\end{enumerate}


При смешенном подходе агенты, выступающие в роли локальных координаторов,
должны быть способны решать задачи с параметрической и
функционально-структурной неопределенностью. При этом исходная задача,
поставленная перед совокупностью таких агентов-координаторов, в целом, может
обладать неопределенностью свыше указанной (неопределенностью по статической
структуре оригинала).

К настоящему времени теория и практика, особенно децентрализованного и
смешенного, подходов к решению сложных задач переживает период становления.

