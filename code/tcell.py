#!/usr/bin/env python
# coding: utf-8
"""
    tcell
    ~~~~~

    ������ ������ ���������� ����� T-���������.

    :copyright: 2013, Igor Kalnitsky
    :license: MIT
"""
import math


class TCell:
    # ��������� �������� ��� ������ ���������� ������
    EPSILON = 0.8

    @classmethod
    def _distance(antibody, antigen):
        x_part = (antibody.x - antigen.x) ** 2
        y_part = (antibody.y - antigen.y) ** 2
        return math.sqrt(x_part + y_part)

    @classmethod
    def _difference(antibody, antigen):
        return 42

    @classmethod
    def _affinity(antibody, antigen, w1, w2):
        diff = w1 * 1 / TCell._distance(antibody, antigen)
        dist = w2 * 1 / TCell._difference(antibody, antigen)
        return diff + dist

    @classmethod
    def affinity(antibody, antigen):
        return TCell._affinity(antibody, antigen, 1, 1)

    @classmethod
    def is_compitent(antibody, antigen):
        return TCell.affinity(antibody, antigen) >= TCell.EPSILON
