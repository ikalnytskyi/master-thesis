# coding: utf-8
"""
    utils
    ~~~~~

    ���� ������ �������� ��������������� �������.

    :copyright: (c) 2013 by Igor Kalnitsky.
    :license: MIT
"""


def itersubclasses(cls, _seen=None):
    """
        ����������� �� ���� �������-�����������.

        :param cls: ������� �����
        :param _seen: ��������� ���������� ��� �������� ���������� �������
    """
    # �������� ���������� ��� ������ ����� ������������� ������
    _seen = _seen or set()

    # �������� �� ������� �����������
    subclasses = cls.__subclasses__()
    for subclass in subclasses:
        if subclass not in _seen:
            # ���������� ������� �����
            _seen.add(subclass)
            yield subclass

            # �������� �� ������� ����������� ������� �����������
            for subclass in itersubclasses(subclass, _seen):
                yield subclass
