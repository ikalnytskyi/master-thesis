#!/usr/bin/env python
# coding: utf-8
"""
    environment
    ~~~~~~~~~~~

    ������ ������ ���������� �����-��������� ������������� �����.

    :copyright: 2013, Igor Kalnitsky
    :license: MIT
"""
import random

from .agent import Agent
from .utils import itersubclasses


class Environment:
    """
        �����-�������� ������������ ��������� �������������� �����.
        ������� ������� � ���������� �� ���������.
    """
    def __init__(self, width, height, agents_amount):
        self.width = width
        self.height = height

        # ������ �� ������� � �������
        self._agents = {}

        # �������� ��������� ��������� ��������
        self._fill_with_agents(agents_amount)

    def _fill_with_agents(self, agents_amount):
        # ������� ������ ���� ����� ������ ������
        agent_classes = [cls for cls in itersubclasses(Agent)]
        # ��������� ���-�� ������� �� ��������
        agents_per_team = agents_amount / 2

        for team in (Agent.Team.RED, Agent.Team.BLUE):
            # ������� ������� �������� `team`
            for i in range(0, agents_per_team):
                x = random.randint(0, self.width)
                y = random.randint(0, self.height)

                # x,y ������ ���� ���������� �����������
                while self._agents.get((x, y)) is not None:
                    x = random.randint(0, self.width)
                    y = random.randint(0, self.height)

                cls = random.choice(agent_classes)
                self._agents[(x, y)] = cls(x, y, Agent.Team.RED)

    def exist_tasks(self):
        red = Agent.Team.RED
        red_agents = [v for k, v in self._agents.items() if v.team == red]

        blue = Agent.Team.BLUE
        blue_agents = [v for k, v in self._agents.items() if v.team == blue]

        # ���� ���� ������ ���� �������
        if red_agents and blue_agents:
            return True
        return False

    def simulate(self):
        for pos, agent in self._agents.items():
            # ������ �����
            tasks = (v for k, v in self._agents.items() if v.team != agent.team)
            # ������, ������� ����� ���������
            tasks = (t for t in tasks if agent.explore(t))
            # ���� ���� ������, ������� ����� ���������
            if tasks and agent.try_destroy(next(tasks)):
                agent.destroy(next(tasks))
