#!/usr/bin/env python
# coding: utf-8
"""
    main
    ~~~~

    ����� ����� ���������. ������� ���������, �������� ����������� �������
    � �������� �������������.

    :copyright: 2013, Igor Kalnitsky
    :license: MIT
"""
import sys

from environment import Environment


def main(arguments):
    # ������� ��������� �������� 20x20
    env = Environment(20, 20)

    # ���������� ����� � ������� ���� ������
    while env.exist_tasks():
        env.simulate()


if __name__ == '__main__':
    main(sys.argv)
