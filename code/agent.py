# coding: utf-8
"""
    agents
    ~~~~~~

    ������ ������ �������� ����������� ���� ������������
    � ������������ �������.

    ������ ������������ ���������� ��������� �������:

    * 1� � ��������������� �����;
    * 2� � ���������������� �����;
    * 3� � ����� �� �������� ��������;
    * 4� � ����� �� ��������� ��������;
    * 5� � ������������ �����;
    * 6� � ������������ �����.

    ������ ���������� ���������� ��������� �������:

    * 1� � ���������� � ���������������� ����;
    * 2� � ���������� � ����������������� ����;
    * 3� � �������� �������;
    * 4� � ��������� �������;
    * 5� � ���������� � ������������ ������;
    * 6� � ���������� � ������������ ������.

    :copyright: (c) 2013, Igor Kalnitsky
    :license: MIT
"""
import abc

from ais import ainet
from .tcell import TCell


class Agent(metaclass=abc.ABCMeta):
    """
        ������� ����� ��� ���� �������.
        ������������� ��������� ��������������.
    """
    class Team:
        """
            ��������� �����, ��������������� ��������� ��� ���������
            ���������� ����.
        """
        RED = 1
        BLUE = 2

    def __init__(self, x, y, team):
        """
            ������������� ������. ��������������� ��� ��������� (��������)
            � ��������� �������� `������������` � `����������`.
        """
        self.x = x
        self.y = y
        self.team = team

        self.copabilities = self._capabilities
        self.complexity = self._complexity

        # ������� �-��������
        self._tcell = TCell

    def try_destroy(self, antigen):
        """
            ���������: ����� �� ��������� ������?
        """
        return self._tcell.is_compitent(self, antigen)

    def destroy(self, antigen):
        """
            ��������� ������.
        """
        self._maturate()

        if self._tcell.affinity(self, antigen) < 1.0:
            self.emit_help(antigen.complexity)
            self.wait_for_help()

        self.perform_job(antigen)

    @abc.abstractproperty
    def _capabilities(self):
        """
            ���������� ��������� �������� `������������`.
            ������ ���� ��������� � ������ ������ ������.
        """
        pass

    @abc.abstractproperty
    def _complexity(self):
        """
            ���������� ��������� �������� `����������`.
            ������ ���� ��������� � ������ ������ ������.
        """
        pass

    def _communcation_range(self):
        """
            ���������������� ���������.
        """
        return 10

    def _sensetive_range(self):
        """
            ��������� ���������.
        """
        return 5


class AntiAirCraftGun(Agent):
    """
        �������� ������.
    """
    def _capabilities(self):
        return "010101"

    def _complexity(self):
        return "011011"


class Tank(Agent):
    """
        ����.
    """
    def _capabilities(self):
        return "011001"

    def _complexity(self):
        return "011011"


class RadarStation(Agent):
    """
        ���������������� �������.
    """
    def _capabilities(self):
        return "000000"

    def _complexity(self):
        return "011011"


class BTR(Agent):
    """
        ����������������.
    """
    def _capabilities(self):
        return "101010"

    def _complexity(self):
        return "011010"


class BMP(Agent):
    """
        ������ ������ ������.
    """
    def _capabilities(self):
        return "101010"

    def _complexity(self):
        return "011010"


class Aviation(Agent):
    """
        �������.
    """
    def _capabilities(self):
        return "111101"

    def _complexity(self):
        return "010111"


class Soldier(Agent):
    """
        �������.
    """
    def _capabilities(self):
        return "101010"

    def _complexity(self):
        return "111011"


class Sniper(Agent):
    """
        �������.
    """
    def _capabilities(self):
        return "101001"

    def _complexity(self):
        return "111010"


class Sapper(Agent):
    """
        �����.
    """
    def _capabilities(self):
        return "101010"

    def _complexity(self):
        return "111011"


class Gunner(Agent):
    """
        ����������.
    """
    def _capabilities(self):
        return "011010"

    def _complexity(self):
        return "111011"


class Thrower(Agent):
    """
        �������������.
    """
    def _capabilities(self):
        return "011101"

    def _complexity(self):
        return "111011"
