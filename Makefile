# Copyright 2012 Zoresvit (c) <zoresvit@gmail.com>
# 
# Latex document build Makefile
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# set TARGET to the name of the main file without the .tex
# (the default value 'document' is overridden by the command line value)
TARGET=main

# latex output filetype (pdf or dvi)
OUTPUT = pdf

# directory for latex auxiliary and temporary files
AUX_DIR = textemp

PARAMS = -output-directory=$(AUX_DIR) -output-format=$(OUTPUT) -file-line-error -interaction=nonstopmode
TEX = pdflatex $(PARAMS)

# directories and files to exclude from compressing into tar
EXCLUDE_PATTERN = --exclude=$(AUX_DIR)

makedoc : 
	mkdir -p $(AUX_DIR)
	# check for bibliography file and execute bibtex if needed
	@if [ -f *.bib ] ; \
		then echo "processing bibliography..." ; \
		$(TEX) $(TARGET).tex ; bibtex $(AUX_DIR)/$(TARGET) ; $(TEX) $(TARGET).tex \
		else echo "no bibliography found!" ; \
		fi
	# run latex several times until all references are resolved from *.aux files
	@echo "rebuilding until all references resolved..."
	@while ($(TEX) $(TARGET).tex ; \
		grep -q "Rerun to get cross-references" $(AUX_DIR)/$(TARGET).log ); do true; \
		done
ifeq ($(OUTPUT), 'dvi')
	@mv $(AUX_DIR)/$(TARGET).dvi ./
	@dvips $(TARGET).dvi
	@ps2pdf $(TARGET).ps
else
	@mv $(AUX_DIR)/$(TARGET).pdf ./
endif

clean :
	rm -r $(AUX_DIR)

cleanall :
	rm -r $(AUX_DIR) *.pdf *.ps *.dvi *.tar.gz

# create compressed archive with complete file set 
# (tex, styles, bibliography and final documents)
tarball:
	tar -czvf $(TARGET).tar.gz $(EXCLUDE_PATTERN) * 
